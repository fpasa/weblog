package main

import (
	_ "embed"
	"errors"
	"fmt"
	"html/template"
	"image"
	_ "image/jpeg"
	_ "image/png"
	"io"
	"log"
	"net/http"
	"os"
	"path"
	"strconv"
	"strings"

	"github.com/julienschmidt/httprouter"
	"gorm.io/gorm"
)

const (
	IMAGES_PAGE_SIZE = 50
)

//go:embed admin/images.html
var imageListPage string
var imageListPageTmpl *template.Template

//go:embed admin/edit_image.html
var editImagePage string
var editImagePageTmpl *template.Template

func init() {
	var err error
	imageListPageTmpl, err = template.New("imageListPage").Funcs(templateFuncs).Parse(imageListPage)
	if err != nil {
		log.Fatalf("Error while parsing imageListPage: %s", err)
	}
	editImagePageTmpl, err = template.New("editImagePage").Funcs(templateFuncs).Parse(editImagePage)
	if err != nil {
		log.Fatalf("Error while parsing editImagePage: %s", err)
	}
}

func ImageList(
	w http.ResponseWriter,
	r *http.Request,
	params httprouter.Params,
) {
	pageNum, err := strconv.Atoi(r.URL.Query().Get("page"))
	if err != nil {
		pageNum = 1
	}
	pageNum -= 1

	db := openDB()
	query := db.Model(&Image{}).Order("created_at DESC")

	mime := r.URL.Query().Get("mime")
	if mime != "" {
		query = query.Where("mime_type = ?", mime)
	}

	var imageCount int64
	result := query.Count(&imageCount)
	if result.Error != nil {
		httpError(w, http.StatusInternalServerError)
		log.Printf("Error counting images: %s", result.Error)
		return
	}

	var images []Image
	result = query.Limit(IMAGES_PAGE_SIZE).Offset(pageNum * IMAGES_PAGE_SIZE).Find(&images)
	if result.Error != nil {
		httpError(w, http.StatusInternalServerError)
		log.Printf("Error reading images: %s", result.Error)
		return
	}

	data := struct {
		Images []Image
		Pages  []int
	}{
		Images: images,
		Pages:  getPages(int(imageCount), IMAGES_PAGE_SIZE),
	}

	err = imageListPageTmpl.Execute(w, data)
	if err != nil {
		httpError(w, http.StatusInternalServerError)
		log.Fatalf("Error while executing template: %s", err)
	}
}

func EditImage(
	w http.ResponseWriter,
	r *http.Request,
	params httprouter.Params,
) {
	imageId, err := strconv.Atoi(params.ByName("id"))
	if err != nil {
		httpError(w, http.StatusBadRequest)
		return
	}

	var image Image

	db := openDB()
	result := db.First(&image, imageId)
	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			http.Redirect(w, r, "/admin/images", http.StatusTemporaryRedirect)
			return
		}

		httpError(w, http.StatusInternalServerError)
		log.Printf("Error fetching image: %s", result.Error)
		return
	}

	err = editImagePageTmpl.Execute(w, image)
	if err != nil {
		httpError(w, http.StatusInternalServerError)
		log.Fatalf("Error while executing template: %s", err)
	}
}

func SaveImage(
	w http.ResponseWriter,
	r *http.Request,
	params httprouter.Params,
) {
	imageId, err := strconv.Atoi(params.ByName("id"))
	if err != nil {
		httpError(w, http.StatusBadRequest)
		return
	}

	var image Image

	db := openDB()
	result := db.First(&image, imageId)
	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			http.Redirect(w, r, "/admin/images", http.StatusSeeOther)
			return
		}

		httpError(w, http.StatusInternalServerError)
		log.Printf("Error fetching image: %s", result.Error)
		return
	}

	err = r.ParseForm()
	if err != nil {
		httpError(w, http.StatusBadRequest)
		log.Printf("Error parsing form: %s", err)
		return
	}

	newSlug := r.FormValue("slug")
	if image.Slug != newSlug {
		srcPath := getImagePath(image.Slug, image.MimeType)
		dstPath := getImagePath(newSlug, image.MimeType)
		err := os.Rename(srcPath, dstPath)
		if err != nil {
			httpError(w, http.StatusInternalServerError)
			log.Printf("Error moving image : %s", err)
			return
		}

		srcSmallPath := getSmallThumbnailPath(image.Slug, image.MimeType)
		dstSmallPath := getSmallThumbnailPath(newSlug, image.MimeType)
		err = os.Rename(srcSmallPath, dstSmallPath)
		if err != nil {
			httpError(w, http.StatusInternalServerError)
			log.Printf("Error moving image : %s", err)
			return
		}

		srcLargePath := getLargeThumbnailPath(image.Slug, image.MimeType)
		dstLargePath := getLargeThumbnailPath(newSlug, image.MimeType)
		err = os.Rename(srcLargePath, dstLargePath)
		if err != nil {
			httpError(w, http.StatusInternalServerError)
			log.Printf("Error moving image : %s", err)
			return
		}
	}

	image.Slug = newSlug
	image.Alt = r.FormValue("alt")
	image.Description = r.FormValue("description")

	result = db.Save(&image)
	if result.Error != nil {
		httpError(w, http.StatusInternalServerError)
		log.Printf("Error saving image: %s", result.Error)
		return
	}

	http.Redirect(
		w, r,
		fmt.Sprintf("/admin/images/%d", image.ID),
		http.StatusSeeOther,
	)
}

func UploadImages(
	w http.ResponseWriter,
	r *http.Request,
	params httprouter.Params,
) {
	// Maximum upload of 16 MB files
	err := r.ParseMultipartForm(16 << 20)
	if err != nil {
		http.Error(w, "File size too large", http.StatusBadRequest)
		return
	}

	for _, fileHeader := range r.MultipartForm.File["images"] {
		src, err := fileHeader.Open()
		if err != nil {
			httpError(w, http.StatusInternalServerError)
			log.Printf("Error opening file header: %s", err)
			return
		}
		defer src.Close()

		first128bytes := make([]byte, 128)
		_, err = src.Read(first128bytes)
		if err != nil {
			httpError(w, http.StatusInternalServerError)
			log.Printf("Error reading the first 128 bytes of file: %s", err)
			return
		}

		_, err = src.Seek(0, io.SeekStart)
		if err != nil {
			httpError(w, http.StatusInternalServerError)
			log.Printf("Error seeking to the beginning of file: %s", err)
			return
		}

		filetype := http.DetectContentType(first128bytes)
		if filetype != "image/jpeg" && filetype != "image/png" {
			http.Error(w, "File must be a JPEG or PNG image.", http.StatusBadRequest)
			return
		}

		name := fileHeader.Filename
		ext := path.Ext(name)
		slug := strings.TrimSuffix(fileHeader.Filename, ext)

		db := openDB()

		var existingImage Image
		result := db.Where("slug = ?", slug).First(&existingImage)
		if result.Error != nil {
			if !errors.Is(result.Error, gorm.ErrRecordNotFound) {
				httpError(w, http.StatusInternalServerError)
				log.Printf("Error fetching image: %s", result.Error)
				return
			}
		}

		// Image already found, find the necessary suffix
		if result.RowsAffected > 0 {
			var existingImages []Image
			result := db.Where("slug LIKE ?", fmt.Sprintf("%s%%", slug)).Find(&existingImages)
			if result.Error != nil {
				httpError(w, http.StatusInternalServerError)
				log.Printf("Error fetching images: %s", result.Error)
				return
			}

			suffix := strconv.Itoa(len(existingImages))
			slug = fmt.Sprintf("%s_%s", slug, suffix)
			name = fmt.Sprintf("%s%s", slug, ext)
		}

		decodedImage, _, err := image.Decode(src)
		if err != nil {
			http.Error(w, "Cannot decode image", http.StatusBadRequest)
			log.Printf("Error decoding image: %s", result.Error)
			return
		}

		_, err = src.Seek(0, io.SeekStart)
		if err != nil {
			httpError(w, http.StatusInternalServerError)
			log.Printf("Error seeking to the beginning of file: %s", err)
			return
		}

		bounds := decodedImage.Bounds()

		image := Image{
			Slug:     slug,
			Width:    uint(bounds.Dx()),
			Height:   uint(bounds.Dy()),
			Size:     uint(fileHeader.Size),
			MimeType: filetype,
		}
		result = db.Create(&image)
		if result.Error != nil {
			httpError(w, http.StatusInternalServerError)
			log.Printf("Error creating image: %s", result.Error)
			return
		}

		dst, err := os.Create(path.Join(IMAGE_PATH, name))
		if err != nil {
			httpError(w, http.StatusInternalServerError)
			log.Printf("Error creating file: %s", err)
			return
		}
		defer dst.Close()

		_, err = io.Copy(dst, src)
		if err != nil {
			httpError(w, http.StatusInternalServerError)
			log.Printf("Error copying uploaded file content: %s", err)
			return
		}

		generateThumbnails([]string{name})
	}

	http.Redirect(w, r, "/admin/images", http.StatusSeeOther)
}

func DeleteImage(
	w http.ResponseWriter,
	r *http.Request,
	params httprouter.Params,
) {
	imageId, err := strconv.Atoi(params.ByName("id"))
	if err != nil {
		httpError(w, http.StatusBadRequest)
		return
	}

	var image Image

	db := openDB()
	result := db.First(&image, imageId)
	if result.Error != nil {
		httpError(w, http.StatusBadRequest)
		log.Printf("Error fetching image: %s", result.Error)
		return
	}

	result = db.Delete(&image)
	if result.Error != nil {
		httpError(w, http.StatusInternalServerError)
		log.Printf("Error deleting image: %s", result.Error)
		return
	}

	imagePath := getImagePath(image.Slug, image.MimeType)
	err = os.Remove(imagePath)
	if err != nil {
		httpError(w, http.StatusInternalServerError)
		log.Printf("Error deleting image file: %s", result.Error)
		return
	}

	smallPath := getSmallThumbnailPath(image.Slug, image.MimeType)
	err = os.Remove(smallPath)
	if err != nil {
		httpError(w, http.StatusInternalServerError)
		log.Printf("Error deleting small thumbnail: %s", result.Error)
		return
	}

	largePath := getLargeThumbnailPath(image.Slug, image.MimeType)
	err = os.Remove(largePath)
	if err != nil {
		httpError(w, http.StatusInternalServerError)
		log.Printf("Error deleting large thumbnail: %s", result.Error)
		return
	}

	http.Redirect(w, r, "/admin/images", http.StatusSeeOther)
}
