package main

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"os"

	"github.com/julienschmidt/httprouter"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

const (
	DATA_PATH         = "./data"
	DB_PATH           = "./data/blog.db"
	TEMPLATE_PATH     = "./data/templates"
	IMAGE_PATH        = "./data/images"
	OUTPUT_PATH       = "./dist"
	CONFIG_PATH       = "./data/config.json"
	USER              = "maria"
	PASSWORD          = "123"
	GENERATOR_WORKERS = 4
)

func main() {
	prepareEnvironment()

	router := httprouter.New()
	router.POST("/api/posts/:id", BasicAuth(SavePage))

	router.GET("/admin/posts", BasicAuth(PostList))
	router.POST("/admin/posts", BasicAuth(NewPost))
	router.GET("/admin/posts/:id", BasicAuth(EditPost))
	router.POST("/admin/delete_post/:id", BasicAuth(DeletePost))
	router.POST("/admin/restore_post/:id", BasicAuth(RestorePost))

	router.GET("/admin/images", BasicAuth(ImageList))
	router.POST("/admin/images", BasicAuth(UploadImages))
	router.GET("/admin/images/:id", BasicAuth(EditImage))
	router.POST("/admin/images/:id", BasicAuth(SaveImage))
	router.POST("/admin/delete_image/:id", BasicAuth(DeleteImage))

	router.GET("/admin/config", BasicAuth(Settings))
	router.POST("/admin/config", BasicAuth(SaveSettings))

	router.GET("/admin/", BasicAuth(PostList))
	router.GET("/admin/style.css", BasicAuth(StyleFile))
	router.GET("/admin/editor.js", BasicAuth(EditorScript))

	router.ServeFiles("/dist/*filepath", http.Dir(OUTPUT_PATH))

	log.Println("Starting server at :8080")
	err := http.ListenAndServe(":8080", router)
	log.Fatalf("Error starting server: %s", err)
}

func prepareEnvironment() {
	err := os.MkdirAll(DATA_PATH, 0750)
	if err != nil {
		log.Fatalf("Error while creating data directory: %s", err)
	}
	err = os.MkdirAll(IMAGE_PATH, 0750)
	if err != nil {
		log.Fatalf("Error while creating image directory: %s", err)
	}
	err = os.MkdirAll(TEMPLATE_PATH, 0750)
	if err != nil {
		log.Fatalf("Error while creating template directory: %s", err)
	}

	if _, err := os.Stat(CONFIG_PATH); errors.Is(err, os.ErrNotExist) {
		err := writeConfigFile(defaultConfigPage)
		if err != nil {
			log.Fatalf("Error while writing default config file: %s", err)
		}
	}

	configFile, err := os.Open(CONFIG_PATH)
	if err != nil {
		log.Fatalf("Error while opening config file: %s", err)
	}
	err = json.NewDecoder(configFile).Decode(&config)
	if err != nil {
		log.Fatalf("Invalid configuration file '%s': %s", CONFIG_PATH, err)
	}

	db := openDB()
	db.AutoMigrate(&Post{})
	db.AutoMigrate(&Image{})

	go generateBlog()
}

func openDB() *gorm.DB {
	db, err := gorm.Open(sqlite.Open(DB_PATH), &gorm.Config{})
	if err != nil {
		log.Fatalf("Error while opening database: %s", err)
	}
	return db
}
