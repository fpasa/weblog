package main

import (
	"database/sql"

	"gorm.io/gorm"
)

type Post struct {
	gorm.Model
	PublishedAt     sql.NullTime
	Status          string
	PostType        string
	Slug            string
	Title           string
	Author          string
	Excerpt         string
	Content         string
	CommentCount    uint64
	Pings           string
	Tags            string
	FeaturedImageID int
	FeaturedImage   Image
}

type Image struct {
	gorm.Model
	Slug        string
	Alt         string
	Description string
	Width       uint
	Height      uint
	Size        uint
	MimeType    string
	Pings       string
}
