package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"
)

var imageOutputPath string

func init() {
	var err error
	imageOutputPath, err = filepath.Abs(path.Join(OUTPUT_PATH, "images"))
	if err != nil {
		log.Fatalf("Error while resolving image output path: %s", err)
	}
}

func httpError(w http.ResponseWriter, code int) {
	http.Error(w, http.StatusText(code), code)
}

func getImagePath(slug string, mimeType string) string {
	return getImagePathBase(IMAGE_PATH, slug, mimeType)
}

func getSmallThumbnailPath(slug string, mimeType string, base ...string) string {
	slug = fmt.Sprintf("%s_small", slug)
	if mimeType == "image/png" {
		mimeType = "image/webp"
	}
	finalBase := imageOutputPath
	if len(base) > 0 {
		finalBase = base[0]
	}
	return getImagePathBase(finalBase, slug, mimeType)
}

func getLargeThumbnailPath(slug string, mimeType string, base ...string) string {
	slug = fmt.Sprintf("%s_large", slug)
	if mimeType == "image/png" {
		mimeType = "image/webp"
	}
	finalBase := imageOutputPath
	if len(base) > 0 {
		finalBase = base[0]
	}
	return getImagePathBase(finalBase, slug, mimeType)
}

func getImagePathBase(base string, slug string, mimeType string) string {
	var name string
	if mimeType == "image/jpeg" {
		name = fmt.Sprintf("%s.jpg", slug)
	} else if mimeType == "image/webp" {
		name = fmt.Sprintf("%s.webp", slug)
	} else {
		name = fmt.Sprintf("%s.png", slug)
	}
	return path.Join(base, name)
}

func writeConfigFile(configValue []byte) error {
	configFile, err := os.Create(CONFIG_PATH)
	if err != nil {
		return err
	}
	defer configFile.Close()
	_, err = configFile.Write(configValue)
	return err
}
