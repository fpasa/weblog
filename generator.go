package main

import (
	"errors"
	"fmt"
	"html/template"
	"log"
	"os"
	"os/exec"
	"path"
	"strings"
	"time"

	"gorm.io/gorm"
)

func prepareOutputDirectory() {
	if _, err := os.Stat(OUTPUT_PATH); errors.Is(err, os.ErrNotExist) {
		err := os.MkdirAll(OUTPUT_PATH, 0750)
		if err != nil {
			log.Fatalf("Error while creating output directory: %s", err)
		}
	}

	if _, err := os.Stat(imageOutputPath); errors.Is(err, os.ErrNotExist) {
		err = os.MkdirAll(imageOutputPath, 0750)
		if err != nil {
			log.Fatalf("Error while creating output directory: %s", err)
		}
	}
}

type Templates struct {
	home *template.Template
	post *template.Template
}

func loadTemplates() *Templates {
	home, err := template.ParseFiles(
		path.Join(TEMPLATE_PATH, "home.html"),
	)
	if err != nil {
		log.Fatalf("Error while parsing post template: %s", err)
	}
	post, err := template.ParseFiles(
		path.Join(TEMPLATE_PATH, "post.html"),
	)
	if err != nil {
		log.Fatalf("Error while parsing post template: %s", err)
	}

	return &Templates{
		home: home,
		post: post,
	}
}

func copyFiles(srcPath string, dstPath string, excludeExt string) {
	if _, err := os.Stat(dstPath); errors.Is(err, os.ErrNotExist) {
		err := os.MkdirAll(dstPath, 0750)
		if err != nil {
			log.Fatalf("Error while creating copy destination folder: %s", err)
		}
	}

	fileNames, err := listDir(srcPath)
	if err != nil {
		log.Fatalf("Error while listing source files: %s", err)
	}

	for _, fileName := range fileNames {
		if excludeExt != "" && strings.HasSuffix(fileName, excludeExt) {
			continue
		}

		log.Printf("Copying file %s", fileName)
		err := copyFile(
			path.Join(srcPath, fileName),
			path.Join(dstPath, fileName),
		)
		if err != nil {
			log.Fatalf(
				"Error while copying %s/%s to %s/%s: %s",
				srcPath, fileName, dstPath, fileName, err,
			)
		}
	}
}

func copyFile(srcPath string, dstPath string) error {
	src, err := os.Open(srcPath)
	if err != nil {
		return err
	}
	defer src.Close()

	dst, err := os.Create(dstPath)
	if err != nil {
		return err
	}
	defer dst.Close()

	dst.ReadFrom(src)
	return nil
}

func listDir(path string) ([]string, error) {
	var files []string

	dir, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer dir.Close()

	fileInfos, err := dir.Readdir(-1)
	if err != nil {
		return nil, err
	}

	for _, fileInfo := range fileInfos {
		if fileInfo.IsDir() {
			continue
		}
		files = append(files, fileInfo.Name())
	}

	return files, nil
}

func generateBlog() {
	log.Printf(
		"### Starting blog generation with %d workers...",
		GENERATOR_WORKERS,
	)
	start := time.Now()

	templates := loadTemplates()
	db := openDB()

	prepareOutputDirectory()

	postCount := generateHTML(db, templates)
	generateHome(db, templates)
	// Copy other template files
	copyFiles(TEMPLATE_PATH, OUTPUT_PATH, ".html")

	log.Println("### Generating thumbnails...")
	thumbnailCount := generateAllThumbnails()

	elapsed := time.Since(start)
	log.Printf(
		"### Generated %d pages and %d thumbnails in about %s",
		postCount, thumbnailCount, elapsed,
	)
}

func generateHTML(db *gorm.DB, templates *Templates) int {
	queue := make(chan Post)
	for i := 0; i <= GENERATOR_WORKERS; i++ {
		go generatorWorker(queue, templates)
	}
	defer close(queue)

	var posts []Post
	var count int

	result := db.Joins("FeaturedImage").FindInBatches(
		&posts,
		100,
		func(tx *gorm.DB, batch int) error {
			count += len(posts)
			for _, post := range posts {
				if post.Status != "published" {
					continue
				}

				queue <- post
			}
			return nil
		},
	)

	if result.Error != nil {
		log.Fatalf("Error while generating blog: %s", result.Error)
	}

	return count
}

func generatorWorker(queue chan Post, templates *Templates) {
	for {
		post, ok := <-queue
		if !ok {
			break
		}

		log.Printf("Generating post '%s' (%d)", post.Title, post.ID)
		generatePost(post, templates)
	}
}

type PostGeneratorData struct {
	Post    *Post
	Content template.HTML
}

func generatePost(post Post, templates *Templates) {
	filePath := path.Join(OUTPUT_PATH, fmt.Sprintf("%s.html", post.Slug))
	file, err := os.Create(filePath)
	if err != nil {
		log.Fatalf("Error while opening output file: %s", err)
	}

	data := PostGeneratorData{
		Post:    &post,
		Content: template.HTML(post.Content),
	}

	err = templates.post.Execute(file, data)
	if err != nil {
		log.Fatalf("Error while executing template: %s", err)
	}
}

type HomeGeneratorData struct {
	Posts []Post
}

func generateHome(db *gorm.DB, templates *Templates) {
	filePath := path.Join(OUTPUT_PATH, "index.html")
	file, err := os.Create(filePath)
	if err != nil {
		log.Fatalf("Error while opening opening file: %s", err)
	}

	data := HomeGeneratorData{}
	result := db.Order("published_at DESC").Limit(12).Joins("FeaturedImage").Find(&data.Posts)
	if result.Error != nil {
		log.Fatalf("Error while fetching recent posts: %s", result.Error)
	}

	err = templates.home.Execute(file, data)
	if err != nil {
		log.Fatalf("Error while executing template: %s", err)
	}
}

func generateAllThumbnails() int {
	imageNames, err := listDir(IMAGE_PATH)
	if err != nil {
		log.Fatalf("Error while listing source files: %s", err)
	}
	return generateThumbnails(imageNames)
}

func generateThumbnails(imageNames []string) int {
	var count int

	options := map[string][]string{
		"small_image/jpeg": []string{
			"--size=480",
			fmt.Sprintf(
				// optimize_coding -> optimize huffman table
				// strip -> strip exif
				"--output=%s/%%s_small.jpg[Q=70,optimize_coding,strip]",
				imageOutputPath,
			),
		},
		"small_image/png": []string{
			"--size=480",
			fmt.Sprintf("--output=%s/%%s_small.webp[Q=80]", imageOutputPath),
		},
		"large_image/jpeg": []string{
			"--size=1200",
			fmt.Sprintf(
				"--output=%s/%%s_large.jpg[Q=75,optimize_coding,strip]",
				imageOutputPath,
			),
		},
		"large_image/png": []string{
			"--size=1200",
			fmt.Sprintf("--output=%s/%%s_large.webp[Q=80]", imageOutputPath),
		},
	}

	for _, imageName := range imageNames {
		ext := path.Ext(imageName)
		name := strings.TrimSuffix(imageName, ext)

		mime := "image/jpeg"
		smallOptionKey := "small_image/jpeg"
		largeOptionKey := "large_image/jpeg"
		if strings.ToLower(ext) == ".png" {
			mime = "image/png"
			smallOptionKey = "small_image/png"
			largeOptionKey = "large_image/png"
		}

		dstPathSmall := getSmallThumbnailPath(name, mime)
		if _, err := os.Stat(dstPathSmall); errors.Is(err, os.ErrNotExist) {
			options[smallOptionKey] = append(
				options[smallOptionKey],
				path.Join(IMAGE_PATH, imageName),
			)
			count += 1
		}

		dstPathLarge := getLargeThumbnailPath(name, mime)
		if _, err := os.Stat(dstPathLarge); errors.Is(err, os.ErrNotExist) {
			options[largeOptionKey] = append(
				options[largeOptionKey],
				path.Join(IMAGE_PATH, imageName),
			)
			count += 1
		}
	}

	for _, opts := range options {
		if len(opts) > 2 {
			stdout, err := exec.Command("vipsthumbnail", opts...).Output()
			if err != nil {
				log.Fatalf(
					"Error executing vipsthumbnail: %s\n%s\n%s",
					err, stdout, err.(*exec.ExitError).Stderr,
				)
			}
		}
	}

	return count
}
