const titleField = document.getElementById("title");
const slugField = document.getElementById("slug");

function autoFillSlug() {
    const title = titleField.value;
    const slug = title
        .replace(/[^a-zA-Z0-9]+/, "-")
        .lower();
}