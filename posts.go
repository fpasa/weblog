package main

import (
	_ "embed"
	"errors"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/julienschmidt/httprouter"
	"gorm.io/gorm"
)

const (
	POSTS_PAGE_SIZE = 25
)

//go:embed admin/posts.html
var postListFile string
var postListTmpl *template.Template

//go:embed admin/editor.html
var editorPage string
var editorPageTmpl *template.Template

//go:embed admin/editor.js
var editorScript string

func init() {
	var err error
	postListTmpl, err = template.New("postListPage").Funcs(templateFuncs).Parse(postListFile)
	if err != nil {
		log.Fatalf("Error while parsing postListPage: %s", err)
	}
	editorPageTmpl, err = template.New("newPagePage").Funcs(templateFuncs).Parse(editorPage)
	if err != nil {
		log.Fatalf("Error while parsing newPagePage: %s", err)
	}
}

func EditorScript(
	w http.ResponseWriter,
	r *http.Request,
	params httprouter.Params,
) {
	w.Header().Add("Content-Type", "text/javascript")
	w.Write([]byte(editorScript))
}

func PostList(
	w http.ResponseWriter,
	r *http.Request,
	params httprouter.Params,
) {
	pageNum, err := strconv.Atoi(r.URL.Query().Get("page"))
	if err != nil {
		pageNum = 1
	}
	pageNum -= 1

	db := openDB()
	query := db.Model(&Post{}).Order("updated_at DESC")

	status := r.URL.Query().Get("status")
	if status != "" {
		query = query.Where("status = ?", status)
		if status == "deleted" {
			query = query.Unscoped()
		}
	}

	search := r.URL.Query().Get("search")
	if search != "" {
		search := fmt.Sprintf("%%%s%%", search)
		query = query.Where(
			`title LIKE ? 
			OR status LIKE ? 
			OR author LIKE ? 
			OR CAST(ID AS TEXT) LIKE ?`,
			search, search, search, search,
		)
	}

	var postCount int64
	result := query.Count(&postCount)
	if result.Error != nil {
		httpError(w, http.StatusInternalServerError)
		log.Printf("Error counting posts: %s", result.Error)
		return
	}

	var posts []Post
	result = query.Limit(POSTS_PAGE_SIZE).Offset(pageNum * POSTS_PAGE_SIZE).Find(&posts)
	if result.Error != nil {
		httpError(w, http.StatusInternalServerError)
		log.Printf("Error reading posts: %s", result.Error)
		return
	}

	data := struct {
		Posts []Post
		Pages []int
	}{
		Posts: posts,
		Pages: getPages(int(postCount), POSTS_PAGE_SIZE),
	}

	err = postListTmpl.Execute(w, data)
	if err != nil {
		httpError(w, http.StatusInternalServerError)
		log.Fatalf("Error while executing template: %s", err)
	}
}

func NewPost(
	w http.ResponseWriter,
	r *http.Request,
	params httprouter.Params,
) {
	post := Post{
		Title:    "untitled",
		Status:   "draft",
		PostType: "post",
	}

	db := openDB()
	result := db.Create(&post)
	if result.Error != nil {
		httpError(w, http.StatusInternalServerError)
		log.Printf("Error creating post: %s", result.Error)
		return
	}

	http.Redirect(w, r, fmt.Sprintf("/admin/posts/%d", post.ID), http.StatusSeeOther)
}

func EditPost(
	w http.ResponseWriter,
	r *http.Request,
	params httprouter.Params,
) {
	postId, err := strconv.Atoi(params.ByName("id"))
	if err != nil {
		httpError(w, http.StatusBadRequest)
		return
	}

	var post Post

	db := openDB()
	result := db.First(&post, postId)
	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			http.Redirect(w, r, "/admin/posts", http.StatusTemporaryRedirect)
			return
		}

		httpError(w, http.StatusInternalServerError)
		log.Printf("Error fetching post: %s", result.Error)
		return
	}

	data := struct {
		Post   Post
		Config Config
	}{
		Post:   post,
		Config: config,
	}

	err = editorPageTmpl.Execute(w, data)
	if err != nil {
		httpError(w, http.StatusInternalServerError)
		log.Fatalf("Error while executing template: %s", err)
	}
}

func DeletePost(
	w http.ResponseWriter,
	r *http.Request,
	params httprouter.Params,
) {
	postId, err := strconv.Atoi(params.ByName("id"))
	if err != nil {
		httpError(w, http.StatusBadRequest)
		return
	}

	db := openDB()
	result := db.Model(&Post{}).Where("id", postId).Updates(
		map[string]interface{}{
			"status":     "deleted",
			"deleted_at": time.Now(),
		},
	)
	if result.Error != nil {
		httpError(w, http.StatusBadRequest)
		log.Printf("Error updating status: %s", result.Error)
		return
	}

	http.Redirect(w, r, "/admin/posts", http.StatusSeeOther)
}

func RestorePost(
	w http.ResponseWriter,
	r *http.Request,
	params httprouter.Params,
) {
	postId, err := strconv.Atoi(params.ByName("id"))
	if err != nil {
		httpError(w, http.StatusBadRequest)
		return
	}

	db := openDB()
	result := db.Model(&Post{}).Unscoped().Where("id", postId).Updates(
		map[string]interface{}{
			"status":     "draft",
			"deleted_at": nil,
		},
	)
	if result.Error != nil {
		httpError(w, http.StatusBadRequest)
		log.Printf("Error updating post: %s", result.Error)
		return
	}

	http.Redirect(w, r, "/admin/posts?status=deleted", http.StatusSeeOther)
}
