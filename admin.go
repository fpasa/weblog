package main

import (
	_ "embed"
	"html/template"
	"math"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

//go:embed admin/style.css
var styleFile []byte

var templateFuncs = template.FuncMap{
	"smallThumbnail": func(image Image) string {
		return getSmallThumbnailPath(image.Slug, image.MimeType, "")
	},
	"largeThumbnail": func(image Image) string {
		return getLargeThumbnailPath(image.Slug, image.MimeType, "")
	},
}

func StyleFile(
	w http.ResponseWriter,
	r *http.Request,
	params httprouter.Params,
) {
	w.Header().Add("Content-Type", "text/css")
	w.Write(styleFile)
}

func getPages(count int, pageSize int) []int {
	var pages []int
	numPages := int(math.Ceil(float64(count) / float64(pageSize)))
	for i := 1; i < numPages; i++ {
		pages = append(pages, i)
	}
	return pages
}
