package main

import (
	_ "embed"
	"encoding/json"
	"html/template"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

//go:embed admin/config.json
var defaultConfigPage []byte

//go:embed admin/settings.html
var settingsPage string
var settingsPageTmpl *template.Template

var config Config

func init() {
	var err error
	settingsPageTmpl, err = template.New("settingsPage").Funcs(templateFuncs).Parse(settingsPage)
	if err != nil {
		log.Fatalf("Error while parsing settingsPage: %s", err)
	}
}

type Config struct {
	WebsiteName string
	WebsiteUrl  string
	Tags        []string
}

func Settings(
	w http.ResponseWriter,
	r *http.Request,
	params httprouter.Params,
) {
	configText, err := json.MarshalIndent(config, "", "    ")
	if err != nil {
		log.Fatalf("Error while encoding configuration as JSON: %s", err)
	}

	data := struct {
		Config     Config
		ConfigText string
	}{
		Config:     config,
		ConfigText: string(configText),
	}

	err = settingsPageTmpl.Execute(w, data)
	if err != nil {
		log.Fatalf("Error while executing template: %s", err)
	}
}

func SaveSettings(
	w http.ResponseWriter,
	r *http.Request,
	params httprouter.Params,
) {
	err := r.ParseForm()
	if err != nil {
		httpError(w, http.StatusBadRequest)
		log.Printf("Error parsing form: %s", err)
		return
	}

	configValue := r.FormValue("config")
	err = json.Unmarshal([]byte(configValue), &config)
	if err != nil {
		http.Error(w, "Invalid JSON configuration", http.StatusBadRequest)
		log.Printf("Invalid JSON configuration: %s", err)
		return
	}

	formattedConfigValue, err := json.MarshalIndent(config, "", "    ")
	if err != nil {
		log.Fatalf("Error while formatting JSON configuration: %s", err)
	}

	err = writeConfigFile(formattedConfigValue)
	if err != nil {
		log.Fatalf("Error while writing formatted config file: %s", err)
	}

	http.Redirect(w, r, "/admin/config", http.StatusSeeOther)
}
