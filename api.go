package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"github.com/julienschmidt/httprouter"
)

func SavePage(
	w http.ResponseWriter,
	r *http.Request,
	params httprouter.Params,
) {
	postId, err := strconv.Atoi(params.ByName("id"))
	if err != nil {
		httpError(w, http.StatusBadRequest)
		return
	}

	var post Post

	err = json.NewDecoder(r.Body).Decode(&post)
	if err != nil {
		httpError(w, http.StatusBadRequest)
		return
	}

	post.ID = uint(postId)

	db := openDB()
	result := db.Save(&post)
	if result.Error != nil {
		httpError(w, http.StatusInternalServerError)
		log.Printf("Error updating post: %s", result.Error)
		return
	}
}
